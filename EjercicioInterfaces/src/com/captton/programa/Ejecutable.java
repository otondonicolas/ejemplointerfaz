package com.captton.programa;
import java.util.ArrayList;

import com.captton.inter.*;

public class Ejecutable {

	public static void main(String[] args) {
		
		
		Ianotador l = new Lapicera();
		l.anotar("hola soy lapicera");

		Lapicera lapi = new Lapicera();
		
		lapi.anotar(" desde instancia Lapicera ");
	
		Ianotador c = new Computadora();
		
		c.anotar("\nSoy compu" );
		
		Computadora compu = new Computadora();
		compu.anotar("\nDesde instancia computadora");
		
		compu.encender();
		
		
		ArrayList<Ianotador> anotadores = new ArrayList<Ianotador>();
		anotadores.add(l);
		anotadores.add(c);
		
		
		System.out.println("----------------------------------");
		
		for(Ianotador i: anotadores) {
			i.anotar("\nel mensaje ");
			
			if(i instanceof Lapicera) {
					System.out.println(" Es Lapicera ");	
			
			}else{
				
				System.out.println(" Es Computadora ");
				
			((Computadora)i).encender();
			
			
			}
			
		
		
		
		}
	
	}

}
